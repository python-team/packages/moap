# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import sys

from moap.util import util, mail
from moap.bug import bug
from moap.doap import doap

class Show(util.LogCommand):
    usage = "[bug id]"
    summary = "show a bug"

    def do(self, args):
        if not args:
            sys.stderr.write('Please give a bug id to show.\n')
            return 3

        bugId = args[0]
        self.debug('Looking up bug %r' % bugId)
        tracker = self.parentCommand.tracker
        try:
            b = tracker.getBug(bugId)
        except bug.NotFoundError, e:
            sys.stderr.write('Bug %s not found in bug tracker.\n' % e.id)
            return 3

        print "%s: %s" % (b.id, b.summary)

class Query(util.LogCommand):
    summary = "query for bugs"
    description = """Query the bug database for bugs.

You can specify an output format string with --format.
Allowed variables are:
  id, summary

Example for trac (using moap's .doap file):
  $ moap doap bug query -f "%(id)s" owner=thomas

Example for bugzilla:
  $ moap bug -U http://bugzilla.gnome.org/ query "product=GStreamer&component=gst-plugins-base&target_milestone=0.10.2"
"""

    def addOptions(self):
        self.parser.add_option('-q', '--query',
            action="store", dest="query",
            help="query string")
        self.parser.add_option('-f', '--format',
            action="store", dest="format",
            help="format string", default="%(id)s: %(summary)s")

    def handleOptions(self, options):
        self.options = options

    def do(self, args):
        if not args:
            sys.stderr.write('Please give a bug search query.\n')
            return 3

        query = args[0]

        self.debug('Looking up bugs')
        tracker = self.parentCommand.tracker
        self.debug('Looking up bug using query %r' % query)
        bugs = tracker.query(query)
        if not bugs:
            sys.stderr.write('Zaroo bugs found.\n')
            return 3


        # scrub the bug dict
        for b in bugs:
            d = {}
            for key in ['id', 'summary']:
                if hasattr(b, key):
                    d[key] = getattr(b, key)
            print self.options.format % d

class Bug(util.LogCommand):
    usage = "[bug-options] %command"
    summary = "interact with bug tracker"
    description = """
This command will interact with the bug tracker at the URL passed with
-U/--URL, or looked up from a .doap file in the current directory.

Currently supported bug trackers:
  trac, bugzilla
"""
    subCommandClasses = [Show, Query]

    def addOptions(self):
        self.parser.add_option('-U', '--URL',
            action="store", dest="URL",
            help="URL of bug tracker")

    def handleOptions(self, options):
        # prefer the URL passed
        # if not passed, try a doap file
        self.URL = options.URL
        if not self.URL:
            from moap.doap import doap
            from moap.command import doap as cdoap
            d = None
            if isinstance(self.parentCommand, cdoap.Doap):
                self.debug('Doap is my parent, looking up its doap file')
                d = self.parentCommand.doap
            else:
                self.debug('Looking for a .doap file in the current dir')
                try:
                    d = doap.findDoapFile(None)
                except doap.DoapException, e:
                    sys.stderr.write(e.args[0])

            if not d:
                return 3

            project = d.getProject()
            self.URL = project.bug_database
        else:
            self.debug('Using specified bug tracker URL %s' % self.URL)

        self.debug('Looking up bug tracker at %s' % self.URL)
        tracker = bug.detect(self.URL)
        if not tracker:
            sys.stderr.write('No known bug tracker found at %s\n' % self.URL)
            return 3

        self.debug('Found bug tracker %r' % tracker)
        self.tracker = tracker
