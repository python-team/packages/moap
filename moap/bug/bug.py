# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Base class for bug tracker abstractions.
"""

import sys

from moap.util import log, util

def detect(URL):
    """
    Detect which bug tracker is being used at the given URL.

    @returns: an instance of a subclass of L{BugTracker}, or None.
    """
    log.debug('bug', 'detecting bug tracker at %s' % URL)
    systems = util.getPackageModules('moap.bug', ignore=['bug', ])

    for s in systems:
        m = util.namedModule('moap.bug.%s' % s)

        try:
            ret = m.detect(URL)
        except AttributeError:
            sys.stderr.write('moap.bug.%s is missing detect()\n' % s)
            continue
         
        if ret:
            try:
                o = m.BugClass(ret)
            except AttributeError:
                sys.stderr.write('moap.bug.%s is missing BugClass()\n' % s)
                continue

            return o
        log.debug('bug', 'did not find %s' % s)

    return None

class BugTracker(log.Loggable):
    def __init__(self, URL):
        self.URL = URL

    def __repr__(self):
        return '<%s instance at %s>' % (str(self.__class__), self.URL)

    def getBug(self, id):
        """
        Get the Bug identified by the given id.
        """
        raise NotImplementedError

    def query(self, queryString):
        """
        Query the database using the given query string.

        @rtype: list of L{Bug}
        """


class Bug(log.Loggable):
    """
    I am a bug in a bug tracker.

    @type id:      str
    @type summary: str
    """
    id = None
    summary = None

    def __init__(self, id, summary):
        self.id = id
        self.summary = summary

class NotFoundError(Exception):
    """
    The bug with the given id was not found.
    """
    def __init__(self, id):
        Exception.__init__(self, id)
        self.id = id
