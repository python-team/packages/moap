# -*- Mode: Python; test-case-name: moap.test.test_bug_bugzilla -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.bug import bugzilla

from moap.test import common

class TestGstCsv(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'bugzilla',
            'gst-plugins-base-0.10.2.csv')
        self.handle = open(file)
        self.bugzilla = bugzilla.Bugzilla(None)

    def testQuery(self):
        l = self.bugzilla.query(None, handle=self.handle)
        for b in l:
            if b.id == '324696':
                return
        self.fail('Did not find bug 324696')

class TestRDFGst:#(common.TestCase):
    try:
        import RDF
    except ImportError:
        skip = "No rdf module, skipping"
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'bugzilla',
            'gst-plugins-base-0.10.rdf')
        #file = os.path.join(os.path.dirname(__file__), 'doap',
        #    'mach.doap')
        self.rdf = bugzilla.BugzillaRDF()
        self.rdf.addLocation('file:' + file)

    def testById(self):
        self.rdf.getById(324696)

try:
    import RDF
except ImportError:
    TestGstCsv.skip = "No rdf module, skipping"
