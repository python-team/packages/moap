# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import tempfile

# use twisted if we can so we get nice trial goodness
try:
    from twisted.trial import unittest
except ImportError:
    import unittest

from moap.util import log
log.init()

class TestCase(unittest.TestCase):
    def setUp(self):
        # In twisted 1.3 Tester sets caseMethodName on the testCase instance
        log.debug('unittest', "%s.setUp() for %s" % (
            self.__class__.__name__,
                getattr(self, '_testMethodName', getattr(
                    self, 'caseMethodName', "[unknown method]"))))

    def tearDown(self):
        log.debug('unittest', "%s.tearDown() for %s" % (
            self.__class__.__name__,
                getattr(self, '_testMethodName', getattr(
                    self, 'caseMethodName', "[unknown method]"))))

class SVNTestCase(TestCase):
    def createRepository(self):
        """
        Create a svn repository we can use for testing.

        @rtype: str
        """
        repodir = self.createDirectory('repo')
        log.debug('unittest', 'creating temp repo in %s' % repodir)
        value = os.system('svnadmin create %s' % repodir)
        self.assertEquals(value, 0, "Could not execute svnadmin")
        return repodir

    def createLive(self):
        """
        Create a "live" area where we can store files for testing.

        @rtype: str
        """
        livedir = self.createDirectory('live')
        log.debug('unittest', 'creating live area in %s' % livedir)
        value = os.system('mkdir -p %s' % livedir)

        self.assertEquals(value, 0, "Could not create %s" % livedir)
        return livedir

    def createDirectory(self, name):
        """
        Create a directory using the given name as part of the name.

        @rtype: str
        """
        return tempfile.mkdtemp(suffix=".%s.svn.test" % name)

    def liveWriteFile(self, livePath, data):
        path = os.path.join(self.livedir, livePath)
        handle = open(path, "w")
        handle.write(data)
        handle.close()
        return path

    def liveCreateDirectory(self, livePath):
        path = os.path.join(self.livedir, livePath)
        os.mkdir(path)
        return path

class FakeStdOut:
    def write(self, what):
        pass
