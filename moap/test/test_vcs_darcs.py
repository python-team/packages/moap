# -*- Mode: Python; test-case-name: moap.test.test_vcs_darcs -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import commands
import tempfile

from moap.vcs import darcs

from moap.test import common

class DarcsTestCase(common.TestCase):
    if os.system('darcs --version > /dev/null 2>&1') != 0:
        skip = "No 'darcs' binary, skipping test."

    def setUp(self):
        self.repository = tempfile.mkdtemp(prefix="moap.test.darcs.repo.")
        os.system('darcs initialize --repodir=%s' % self.repository)
        self.checkout = tempfile.mkdtemp(prefix="moap.test.darcs.checkout.")
        os.rmdir(self.checkout)
        cmd = 'darcs get %s %s' % (self.repository, self.checkout)
        (status, output) = commands.getstatusoutput(cmd)
        self.failIf(status, "Non-null status %r" % status)
        # create _darcs/prefs/author
        a = os.path.join(self.checkout, '_darcs', 'prefs', 'author')
        h = open(a, 'w')
        h.write('e@mail.org')
        h.close()
        
    def tearDown(self):
        os.system('rm -rf %s' % self.checkout)
        os.system('rm -rf %s' % self.repository)
        
class TestDetect(DarcsTestCase):
    def testDetectRepository(self):
        self.failIf(darcs.detect(self.repository))

    def testDetectCheckout(self):
        # should succeed
        self.failUnless(darcs.detect(self.checkout),
            "darcs checkout not detected")

    def testHalfCheckout(self):
        # should fail
        checkout = tempfile.mkdtemp(prefix="moap.test.")
        os.mkdir(os.path.join(checkout, '_darcs'))
        self.failIf(darcs.detect(checkout))
        os.system('rm -rf %s' % checkout)

class TestTree(DarcsTestCase):
    def testDarcs(self):
        v = darcs.VCSClass(self.checkout) 
        self.failUnless(v)

        paths = ['test/test1.py', 'test/test2.py', 'test/test3/test4.py',
            'test5.py', 'test6/', 'test/test7/']
        tree = v.createTree(paths)
        keys = tree.keys()
        keys.sort()
        self.assertEquals(keys, ['', 'test', 'test/test3'])
        self.failUnless('test1.py' in tree['test'])
        self.failUnless('test2.py' in tree['test'])
        self.failUnless('test7' in tree['test'])
        self.failUnless('test4.py' in tree['test/test3'])
        self.failUnless('test5.py' in tree[''], tree[''])
        self.failUnless('test6' in tree[''], tree[''])

class TestIgnore(DarcsTestCase):
    def testGetUnignored(self):
        v = darcs.VCSClass(self.checkout) 
        self.failUnless(v)

        self.assertEquals(v.getUnknown(v.path), [])

        path = os.path.join(self.checkout, 'test')
        handle = open(path, 'w')
        handle.write('test')
        handle.close()

        self.assertEquals(v.getUnknown(v.path), ['test'])

        v.ignore(['test', ])
        
        self.assertEquals(v.getUnknown(v.path), [])

class TestDiff(DarcsTestCase):
    def testDiff(self):
        v = darcs.VCSClass(self.checkout) 
        self.failUnless(v)
        
        self.assertEquals(v.diff(self.checkout), "")

    def testGetChanges(self):
        v = darcs.VCSClass(self.checkout) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_add_one_line.diff')
        d = open(file).read()
        v.getChanges(None, d)

    def testGetChangesMultiple(self):
        v = darcs.VCSClass(self.checkout) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_multiple.diff')
        d = open(file).read()
        v.getChanges(None, d)
  
